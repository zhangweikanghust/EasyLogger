# linux demo

---

## 1、简介

使用GCC编译。通过 `main.c` 的 `test_elog()` 方法来测试日志的输出。已在 `easylogger\inc\elog_cfg.h` 开启异步输出模式。

### 1.1、使用方法

使用前需提前配置好编译环境，配置成功后，执行 make，等待编译完成后，运行 `out\EasyLoggerLinuxDemo` 即可看到运行结果。

## 2、文件（夹）说明

- `easylogger\port\elog_port.c` 移植参考文件
- easylogger\plugins\file\elog_file_port.c File Log功能移植参考文件

## 3、其他功能

可以打开 `main.c` 中的部分注释，来测试以下功能。

- `elog_set_output_enabled(false);` ：动态使能或失能日志输出
- `elog_set_filter_lvl(ELOG_LVL_WARN);` ：动态设置过滤优先级
- `elog_set_filter_tag("main");` ：动态设置过滤标签
- `elog_set_filter_kw("Hello");` ：动态设置过滤关键词

增加注释： 这个demo其实就是按照/docs/zh/port/kernel.md 的说明移植的，
下面引用这个说明
2、将\easylogger\（里面包含inc、src及port的那个）文件夹拷贝到项目中；
3、添加\easylogger\src\elog.c、\easylogger\src\elog_utils.c及\easylogger\port\elog_port.c这些文件到项目的编译路径中（elog_async.c 及 elog_buf.c 视情况选择性添加）；
4、添加\easylogger\inc\文件夹到编译的头文件目录列表中；
demo中目录下就包含了这个拷贝过来的easylogger文件夹，而且根据linux平台的特点作了适配。
